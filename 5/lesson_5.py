#№1
#Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

vowel = set(['а', 'е', 'є', 'и', 'і', 'ї', 'о', 'у', 'ю', 'я'])
user_input = input('Enter the word: ')
cnt = 0
words = user_input.split()
for word in words:
    prev_letter = word[0]
    for letter in word[1:]:
        if prev_letter in vowel and letter in vowel:
            cnt+=1
            break
        prev_letter = letter
print(cnt)

#№2
#Є два довільних числа які відповідають за мінімальну і максимальну ціну.
#Є Dict з назвами магазинів і цінами: { "cito": 47.999,
                                      # "BB_studio" 42.999,
                                      #"momo": 49.999,
                                      # "main-service": 37.245,
                                      #"buy.now": 38.324,
                                      # "x-store": 37.166,
                                      # "the_partner": 38.988,
                                      # "sota": 37.720,
                                      # "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною.

my_dict = { "cito": 47.999,
            "BB_studio": 42.999,
            "momo": 49.999,
            "main-service": 37.245,
            "buy.now": 38.324,
            "x-store": 37.166,
            "the_partner": 38.988,
            "sota": 37.720,
            "rozetka": 38.003
            }
price_min = input('Enter min price: ')
price_max = input('Enter max price: ')
s_shops = []
for key, value in my_dict.items():
    if value > float(price_min) and value < float(price_max):
        s_shops.append(key)
print(s_shops)