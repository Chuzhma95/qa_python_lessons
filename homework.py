from tkinter import *
from tkinter import messagebox

def btn_click():
    # Отримуємо загальну кількість та суму
    total_kg_apple = float(entry1.get())
    total_kg_plum = float(entry3.get())
    total_payment = float(entry4.get())

    # Розрахунок вартості яблук та слив
    apple_cost = total_kg_apple * 15
    plum_cost = total_kg_plum * 20

    # Перевірка, чи достатньо коштів
    total_cost = apple_cost + plum_cost
    change = total_payment - total_cost

    if change < 0:
        messagebox.showerror('Помилка', 'Недостатньо коштів!')
    else:
        grn = int(change)
        kop = int((change - grn) * 100)
        messagebox.showinfo('Ваша решта', str(grn) + ' грн ' + str(kop) + ' коп.')

root = Tk()
root.title('Діалог з касиром')

lab1 = Label(root, text='1. Яблука " Чемпіон" 1 кг - 15 грн.', fg='red')
lab1.grid(row=0, column=0, columnspan=2, sticky=W)

lab2 = Label(root, text='Скільки кг Ви купуєте яблук?')
lab2.grid(row=1, column=0, sticky=W)

lab3 = Label(root, text='2. Слива "Немішаївська" 1 кг - 20 грн.', fg='blue')
lab3.grid(row=2, column=0, columnspan=2, sticky=W)

lab4 = Label(root, text='Скільки кг Ви купуєте слив?')
lab4.grid(row=3, column=0, sticky=W)

lab5 = Label(root, text='Скільки грн Ви сплачуєте?')
lab5.grid(row=4, column=0, sticky=W)

entry1 = Entry(root, width=8)
entry1.grid(row=1, column=1, padx=5)

entry3 = Entry(root, width=8)
entry3.grid(row=3, column=1, padx=5)

entry4 = Entry(root, width=8)
entry4.grid(row=4, column=1, padx=5)

btn = Button(root, text='Сплатити', command=btn_click)
btn.grid(row=5, column=0, columnspan=2, pady=5)

root.mainloop()
