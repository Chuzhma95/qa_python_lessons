# Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"
#
# Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача (1 - рік, 22 - роки, 35 - років і тд...).
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "Незважаючи на те, що вам 42 роки, білетів всеодно нема!"
# Зробіть все за допомогою функцій! Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду.

# def show_happy_message(age):
#     if '7' in age:
#         return 'І вам сьогодні пощастить!'
#     return ''


def word_formatter(age):
    age_int = int(age)
    if age_int == 1:
        return 'рік'
    elif age[-1] == '1':
        if age[-2:] == '11':
            return 'років'
        else:
            return 'рік'
    elif age_int in [12, 13, 14]:
        return 'років'
    elif age[-1] in ['2', '3', '4']:
        return 'роки'
    else:
        return 'років'

def show_age_result(age):
    age_int = int(age)
    age_formatted = word_formatter(age)

    if age_int < 7:
        print(f'Тобі {age_int} {age_formatted} Де твої батьки?')
    elif age_int < 16:
        print(f'Тобі лише, {age_int} {age_formatted} а це є фільм для дорослих')
    elif age_int > 65:
        print(f'Вам {age_int} {age_formatted} Покажіть пенсійне посвідчення!')
    else:
        print(f'Незважаючи на те, що вам {age_int} {age_formatted} білетів всеодно нема!')
    if '7' in age:
        print('І вам сьогодні пощастить!')

user_input = input('Enter your age: ')
show_age_result(user_input)



# # #Вивести на екран кількість викликів функції
# #
# # cnt = 0
# #
# # def decorator1(func):
# #     def warapper(inp):
# #         global cnt
# #         cnt += 1
# #         func(inp)
# #     return warapper
# #
# #
# #
# # @decorator1
# # def function(enter):
# #     print(enter)
# #
# # function(10)
# # function(10)
# # function(10)
# # function(10)
# # function(10)
# #
# # print(cnt)
#
#
# # #Написати декоратор, щоб буде валідувати вхідні значення: перше має бути стрінг, друге інтежер від 1 до 100,
# # третє має бути список з стрінгів
# # в іншому вивести помилку raise ValueError("Validation Error")
#
#
# def decorator2(func):
#     def wrapper(value1, value2, value3):
#         if not isinstance(value1, str):
#             raise ValueError("Validation Error")
#         if not isinstance(value2, int) or not (value2 >= 1 and value2 <= 100):
#             raise ValueError("Validation Error")
#         if not isinstance(value3, list):
#             raise ValueError("Validation Error")
#         for i in value3:
#             if not isinstance(i, str):
#                 raise ValueError("Validation Error")
#         func(value1,value2, value3)
#
#     return wrapper
#
#
# @decorator2
# def function(arg1, arg2, arg3):
#     print('Done')
#
#
# function('Artem', 100, ['asdasda', 'asdasda'])



# Ідеальний декоратор!
# ```
# def decorator(func):
#     @wraps(func)
#     def wrapper(*args, **kwargs):
#         ...
#         return func(*args, **kwargs)
#
#     return wrapper
#


# def decorator_with_parameters(arg1, arg2):
#     def decorator(func):
#         @wraps(func)
#         def wrapper(*args, **kwargs):
#             ...
#             return func(*args, **kwargs)
#
#         return wrapper
#
#     return decorator
