#№1
# Зформуйте строку, яка містить певну інформацію про символ в відомому слові.
# Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

# word = 'Test'
# print(f'The 2 symbol in {word} is', word[1])

word_input = input('Enter words: ')
print (f'The 2 symbol in {word_input} is', word_input[1])

#№2
#Написати цикл, який буде вимагати від користувача ввести слово, в якому є буква "о"
# (враховуються як великі так і маленькі). Цикл не повинен завершитися, якщо користувач ввів слово без букви о.

while True:
    user_input = input('Enter the word with the letter o or O: ')
    if 'o'in user_input or 'O' in user_input:
        print(f'Thanks for {user_input} ')
        break
    print(f'{user_input} - it is not letter o or O')