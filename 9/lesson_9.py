# Task№1
# напишіть функцію, яка перевірить, чи передане їй число є парним чи непарним (повертає True False)

#Task№2
# напишіть функцію. яка приймає стрічку, і визначає, чи дана стрічка відповідає результатам роботи методу capitalize()
# (перший символ є верхнім регістром, а решта – нижнім.) (повертає True False)

#Task№3
# написати до кожної функції мінімум 5 assert

# Task№3
#написати декоратор, який добавляє принт з результатом роботи отриманої функції + текстовий параметр, отриманий ним
# (декоратор з параметром - це там, де три функції)
#при цьому очікувані результати роботи функції не змінюються (декоратор просто добавляє принт)

from functools import wraps

def decorator(argument):
    def dec(function):
        """
        A decorator that adds a print with the result of the function received + the text parameter received by it
        :param function: value function
        :return: decorated function result
        """
        @wraps(function)
        def wrapper(*args, **kwargs):
            result = function(*args, **kwargs)
            print(argument, args)
            print(result)
            return result
        return wrapper
    return dec


@decorator('check_number_even')
def check_number_even(number):
    """
    A function that will check whether the number passed to it is even or odd (returns True False)
    :param number: value type(int or float) function
    :return: decorated function result
    """
    if not isinstance(number, (int, float)):
        raise ValueError('Bad format, try again')
    if number % 2 == 0:
        return True
    else:
        return False


assert check_number_even(3.5) is False
assert check_number_even(5) is False
assert check_number_even(10) is True
assert type(check_number_even(2)) == bool
assert isinstance(check_number_even(3), bool)
assert check_number_even(-5) is False
assert check_number_even(0) is True


@decorator('check_capitalize')
def check_capitalize(text: str):
    """
    A function that accepts a tape and determines whether this tape corresponds to the results of the capitalize() method
    :param text: value type(str) function
    :return: decorated function result
    """
    if text == text.capitalize():
        return True
    else:
        return False


assert check_capitalize('Test') is True
assert check_capitalize('Bad format, try again') is True
assert check_capitalize('Bad Format, Try Again') is False
assert check_capitalize('test') is False
assert check_capitalize('Bad format, Try again') is False
assert check_capitalize('bad format, try again') is False
assert check_capitalize('TEst') is False
assert type(check_capitalize('test')) == bool
assert isinstance(check_capitalize('test'), bool)
assert check_capitalize('10') is True
assert check_capitalize('2.1') is True
assert check_capitalize('[1, 2, 3, 4]') is True
