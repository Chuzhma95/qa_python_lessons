def is_majority(items: list[bool]) -> bool:
    if items.count(True) > items.count(False):
        return True
    else:
        return False


print("Example:")
print(is_majority([True, True, False, False, False]))