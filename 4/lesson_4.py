#№1
# Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for i in lst1:
    if type(i) == str:
        lst2.append(i)
print(lst2)


#№2
# Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.

my_list = [21, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44,]
for i in reversed(my_list):
    if i < 21 or i > 74:
        my_list.remove(i)
print(my_list)

#№3
#Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.

user_input = input('Enter the word with the letter o: ')
words = user_input.split()
cnt = 0
for word in words:
    if word[-1] == 'o':
        cnt += 1
print(cnt)
