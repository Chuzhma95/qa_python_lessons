from abc import ABC, abstractmethod
from typing import Union
from random import randint, choice
from faker import Faker
fake = Faker(locale='uk_UA')


class SchoolPersonal(ABC):

    def __init__(self, name, salary: Union[int, float]):
        self.name = name
        self.salary = salary

    @abstractmethod
    def __str__(self):
        raise NotImplementedError # допомагає не забути що метод не є реалізованим в дочірніх класах


class Teacher(SchoolPersonal):
    def __str__(self):
        return f'Teacher: {self.name}, {self.salary}'


    __repr__ = __str__


class TechnicalStaff(SchoolPersonal):
    def __str__(self):
        return f'Technical Staff: {self.name}, {self.salary}'


class School:

    def __init__(self, name_school: str, director: Teacher, list_teacher: int = 10, list_teh_staff: int = 5):
        self.name_school = name_school
        self.director = director
        self.list_teachers = [Teacher(fake.name(), randint(10000, 50000)) for position in range(list_teacher)]
        self.list_teh_staff = [TechnicalStaff(fake.name(), randint(6000, 20000)) for position in range(list_teh_staff)]

    # @property  #якщо у нас щось змінюється з часом, але є параметром ніби сталим тоді використовуємо property
    # def get_total_salary(self):
    #     all_staff = []
    #     all_staff.append(self.director)
    #     all_staff += self.list_teh_staff
    #     all_staff += self.list_teachers
    #     total = sum((obj.salary for obj in all_staff))
    #     return total

    @property
    def get_month_salary(self):
        month_teacher_salary = sum([teacher.salary for teacher in self.list_teachers])
        month_tech_salary = sum([tech.salary for tech in self.list_teh_staff])
        total = month_teacher_salary + month_tech_salary + self.director.salary
        return total

    def new_director(self):
        old_director = self.director
        new_director = self.list_teachers.pop()
        self.director = new_director
        self.list_teachers.append(old_director)


school1 = School('Nemesh', Teacher('Артем', 15000))
school1.list_teachers.append(Teacher('Олександр', 100000))
school1.new_director()
print(school1.list_teachers)


# shool1 = School('Nemesh', Teacher('Artem', 50000))
