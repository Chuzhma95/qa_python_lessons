import datetime
import uuid
from art import *

class Bank:
    total_deposit = 0

    def __init__(self, name, start_sum, percent):
        self.name = name
        self.amount_money = start_sum #0
        self.__percent = percent
        self.open_data = datetime.date.today()
        self.uuid = uuid.uuid4()
        #self.top_up(start_sum)
        self.increase_total_deposit(start_sum)

    @property
    def percent(self):
        """Допомагає змінювати значення (працювати) з приватні файлами"""
        return self.__percent

    @percent.setter
    def percent(self, percent):
        """Допомагає валідувати приватні файли"""
        self.__percent = percent

    def __str__(self):
        """Виводить дані в більш четабельному форматі"""
        return f'Bank_data : {self.name} {self.amount_money}$, {self.percent}%'

    def top_up(self, sum_money):
        """Метод для поповнення коштів на рахунок"""
        if sum_money >= 1:
            self.amount_money += sum_money
            self.increase_total_deposit(sum_money)

    def wirhdrawal(self, sum_money):
        """Метод для зняття коштів з рахунку"""
        if sum_money > 0 and self.amount_money >= sum_money:
            self.amount_money -= sum_money
            self.decrease_total_deposit(sum_money)

    def __del__(self):
        self.wirhdrawal(self.amount_money)
        print (f'У {self.name} закрито рахунок у звязку з ліквідацією банка, повернуто, власник препензій не має')

    def money_transfer(self, other, summa):
        """Метод для переказу коштів від одного обʼєкта до іншого"""
        self.wirhdrawal(summa)
        other.top_up(summa)

    @classmethod
    def increase_total_deposit(cls, add_sum):
        cls.total_deposit += add_sum

    @classmethod
    def decrease_total_deposit(cls, minus_sum):
        cls.total_deposit -= minus_sum

    @property
    def get_todays_profit(self):
        """Метод який автоматично розраховує плановий прибуток за сьогоднішній день"""
        return (self.percent/365)/100 * self.amount_money

    @staticmethod
    def slogan_bank():
        """Метод який виводить рекламний слоган банку"""
        tprint("NEMISH BANK", "rnd-xlarge")


user_account1 = Bank('Nastya', 500, 10)
del user_account1
user_account2 = Bank('Artem', 100, 3)
user_account3 = Bank('Oleksandr', 300, 4)
user_account3.money_transfer(user_account2, 100)

Bank.slogan_bank()