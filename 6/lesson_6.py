# завдання 1
# урл http://api.open-notify.org/astros.json
# вивести список всіх астронавтів, що перебувають в даний момент на орбіті (дані не фейкові, оновлюються в режимі реального часу)


import requests
from pprint import pprint
from tabulate import tabulate

# url = 'http://api.open-notify.org/astros.json/'
# responce = requests.get(url)
# pprint(responce.json(), width=120)

url = 'http://api.open-notify.org/astros.json/'
response = requests.get(url)
response_json = response.json()
cosmos = response_json['people']
print(tabulate(cosmos, headers='keys', tablefmt='grid'))


#Завдання 2.
# Вивести назву міста, температуру і швидкість вітру з:
# https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'
city_name = input('Enter the city: ')
url = f'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'
weather = requests.get(url) #pprint(weather.json())
response_json = weather.json()
town_name = response_json['name']
temperature = response_json['main']['temp']
speed = response_json['wind']['speed']
print('Town_name ->', town_name, '\nTemperature ->', temperature, '\nWind speed ->', speed, )
