# Напишіть декоратор, який перетворює результат роботи функції на стрінг
# Напишіть докстрінг для цього декоратора

from functools import wraps

def value_go_string(func):
    """
    if we enter any value in our decoratofr our decorator converts that variable to a string
    :param func: input function
    :return: decorated function
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        arg_str = []
        for val in args:
            arg_str.append(str(val))
        kwargs_str = {}
        for key, val in kwargs.items():
            kwargs_str[key] = str(val)
        return func(*arg_str, **kwargs_str)
    return wrapper




@value_go_string
def enter_any(arg1,arg2, arg3):
   print('--->', type(arg1), arg1)
   print('--->', type(arg2), arg2)
   print('--->', type(arg3), arg3)


enter_any(12.3, 4, 'Test')


