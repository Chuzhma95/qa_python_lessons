from tkinter import *
from tkinter import messagebox


def btn_click():
    b1 = float(entry1.get())
    b2 = float(entry2.get())
    b3 = float(entry3.get())
    x = float(entry4.get())
    y = float(entry5.get())
    c = float(entry6.get())

    # Перевірка, чи b1 не дорівнює нулю
    if b1 == 0:
        messagebox.showerror('Помилка', 'b1 не може дорівнювати нулю')
        return

    a = ((35 * x - 4 * y) / b1) - ((8 * c + 5) / (4 * b2)) * (b3 / (x + y))
    r = a
    rez = int(r)

    messagebox.showinfo('Розв\'язання', str(rez))


root = Tk()
root.title("розв'язання")

lab1 = Label(root, text='((35*x-4*y)/b1)-((8*c+5)/(4*b2))*(b3/(x + y))', fg='red')
lab1.grid(row=0, column=0, columnspan=2, sticky=W)
lab2 = Label(root, text='b1 =')
lab2.grid(row=1, column=0, sticky=W)
lab3 = Label(root, text='b2 =')
lab3.grid(row=2, column=0, sticky=W)
lab4 = Label(root, text='b3 =')
lab4.grid(row=3, column=0, sticky=W)
lab5 = Label(root, text='x =')
lab5.grid(row=4, column=0, sticky=W)
lab6 = Label(root, text='y =')
lab6.grid(row=5, column=0, sticky=W)
lab7 = Label(root, text='c =')
lab7.grid(row=6, column=0, sticky=W)

entry1 = Entry(root, width=8)
entry1.grid(row=1, column=1, padx=5)
entry2 = Entry(root, width=8)
entry2.grid(row=2, column=1, padx=5)
entry3 = Entry(root, width=8)
entry3.grid(row=3, column=1, padx=5)
entry4 = Entry(root, width=8)
entry4.grid(row=4, column=1, padx=5)
entry5 = Entry(root, width=8)
entry5.grid(row=5, column=1, padx=5)
entry6 = Entry(root, width=8)
entry6.grid(row=6, column=1, padx=5)

btn = Button(root, text='Вирішити', command=btn_click)
btn.grid(row=7, column=0, columnspan=2, pady=5)

root.mainloop()
